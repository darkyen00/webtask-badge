import app from '../lib/app';
import WebTask from 'webtask-tools';

export default WebTask.fromExpress(app);