// var fs = require('fs');
// var vr = require('verquire');
// var versions = vr.modules;

// function getPkgDep(depName){
//   return JSON.parse(fs.readFileSync(__dirname + '/node_modules/' + depName + '/package.json'));
// }

// function getVersions(depName){
//   return versions[depName];
// }

// function getTopModules(moduleDict, depName){
//   var depObj  = getPkgDep(depName);
//   var depVers = getVersions(depName);
//   if( Array.isArray(depVers) ){
//     moduleDict[depName] = depVers;
//   }else{
//     moduleDict[depName] = moduleDict[depName] || [];
//     moduleDict[depName].push(depObj.version);
//   }
//   return moduleDict;
// }

// function removeDot(dir) {
//   // for .bin removals
//   return dir[0] !== '.';
// }

// module.exports = function(ctx, cb) {

//   var mods = Object.keys(process.binding("natives"))
//     .filter(function(nativeDep) {
//       return nativeDep[0] !== '_';
//     }).reduce(function(natives, native) {
//       natives[native] = ['native'];
//       return natives;
//     }, {});


//   fs.readdirSync(__dirname + '/node_modules')
//     .filter(removeDot)
//     .reduce(getTopModules, mods)

//   // var res = require.resolve('request@2.56.0');

//   cb(null, {
//     node_version: process.version,
//     modules: mods,
//     verquire_list: versions 
//   });
// }
var express = require('express');
var wt = require('webtask-tools');
var app = express();
app.use(function(req, res, next){
  var usableSlug = req.originalUrl.replace(req.path, '');
  console.log(usableSlug);
  next();
});
module.exports = wt.fromExpress(app);