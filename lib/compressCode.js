import UglifyJS from 'uglify-js';
export default function compressCode(code){
  const ast = UglifyJS.parse(code);
  console.log("Scoping");
  ast.figure_out_scope();
  ast.compute_char_frequency();
  console.log("renaming");
  ast.mangle_names();
  console.log("Compressing");
  const comp = UglifyJS.Compressor({ drop_console: true });
  console.log("Yet again");
  ast.transform(comp);
  console.log("building as string");
  return ast.print_to_string();
}
