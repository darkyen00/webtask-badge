var babel = require('babel');
import clone from 'clone';
import through from 'through';
import path from 'path';

function regexify(val, escape) {
  if (!val) {
    return /.^/;
  }

  if (typeof val === 'string') {
    escape = escape || false;

    if (escape) {
      val = require('escape-string-regexp')(val);
    }
    return new RegExp(val);
  }

  if (require('util').isRegExp(val)) {
    return val;
  }

  throw new TypeError('illegal type: ' + val + ' for regexpify');
}

function arrayify(collection, options) {
  if (typeof collection === 'string') return (options && options.query ? toArray(document.querySelectorAll(collection)) : [collection])
  if (typeof collection === 'undefined') return []
  if (collection === null) return [null]
  if (typeof window != 'undefined' && collection === window) return [window]
  if (Array.isArray(collection)) return collection.slice()
  if (typeof collection.length != 'number') return [collection]
  if (typeof collection === 'function') return [collection]
  if (collection.length === 0) return []
  var arr = []
  for (var i = 0; i < collection.length; i++) {
    if (collection.hasOwnProperty(i) || i in collection) {
      arr.push(collection[i])
    }
  }
  if (arr.length === 0) return [collection]
  return arr
}
// this will use the source babel for
// compression
const browserify = function(f,o){
  return browserify.configure(o)(f);
}

browserify.configure = (opts) => {
   opts = opts || {};
   console.log(opts);

   if (opts.sourceMap !== false) opts.sourceMap = "inline" ;
   if (opts.extensions) opts.extensions = arrayify(opts.extensions);
   opts.ignore = regexify(opts.ignore);
   if (opts.only) opts.only = regexify(opts.only);

   return function (filename) {
     console.log("Got new file", filename);
     if (opts.ignore.test(filename) || (opts.only && !opts.only.test(filename)) || !babel.canCompile(filename, opts.extensions)) {
       return through();
     }

     if (opts.sourceMapRelative) {
       filename = path.relative(opts.sourceMapRelative, filename);
     }

     var data = "";

     var write = function (buf) {
       data += buf;
     };

     var end = function () {
       var opts2 = clone(opts);
       delete opts2.sourceMapRelative;
       delete opts2.extensions;
       delete opts2.global;
       opts2.filename = filename;

       try {
         var out = babel.transform(data, opts2).code;
       } catch(err) {
         stream.emit("error", err);
         stream.queue(null);
         return;
       }

       stream.queue(out);
       stream.queue(null);
     };

     var stream = through(write, end);
     return stream;
   };
}

export default browserify;
