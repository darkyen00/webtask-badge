import Express from 'express';
import bodyParser from 'body-parser';
import request from 'request-promise';
import temp from 'temp';
import Promise from 'bluebird';

import Debug from 'debug';
import resolveDependencies from './resolveDependencies';
import getGitRepository from './getGitRepository';
import browserify from 'browserify';
import customBabelify from './customBabelify';
import path from 'path';
import fs from 'fs';
import cp from 'child_process';
import compressCode from './compressCode';

function pathExists(fp){
  var fn = typeof fs.access === 'function' ? fs.access : fs.stat;
	return new Promise(function (resolve) {
		fn(fp, function (err) {
			resolve(!err);
		});
	});
}

Promise.promisifyAll(fs);
// not only it just compiles but also
// it puts it in an amazon bucket
// from where you can actually deploy

// params
//   src
//   type = zip,repo
//   transpile = false
//   file = []
const app = Express();
Promise.promisifyAll(temp);

temp.track();

app.use(bodyParser.json());

app.get('/compile', async function (req, res){
  const {src, type, transpile} = req.query;

  // Is this a github repo ?
  // we cannot clone yet so we have to rely on gh or gitlab
  // for remote zipped package.
  console.log(src, type, transpile);

  if( type === 'repo' ){
    try{
      const dirPath = await temp.mkdirAsync('repoName');
      // once this is don lets roll!
      console.log('Fetching in', dirPath);
      const cloned = await getGitRepository(src, dirPath);

      const pkJsonPath = path.join(dirPath, 'package.json');
      if(!(await pathExists(pkJsonPath))){
        res.json({
          status: 400,
          message: 'Package.json not found in the repo',
          path: dirPath
        });
      }

      // remove babelrc if present
      try{
        await fs.unlinkAsync(path.join(dirPath, '.babelrc'));        
      }catch(e){
        // do nothing it didn't exist 
      }


      // extract dependencies, once we have
      // the dependencies we can then simply
      // use browserify cdn to obtain the package
      // files

      // magic!
      const pkg = require(pkJsonPath);
      const {unresolved, resolved} = resolveDependencies(pkg.dependencies);
      const exeCommand = "npm install --production " + (unresolved.join(" ") || 'ls');
      console.log(exeCommand);
      cp.exec(exeCommand,{ cwd: dirPath }, function(err, ab, bc){
            // now browserify with all dependices
            console.log("NPM installed finished");
            const entryPath  = path.join(dirPath, pkg['x-webtask'] || pkg.main || 'index.js');
            const bundleDir  = path.join(dirPath, 'bundle');
            const bundlePath = path.join(bundleDir, 'app.js');
            const externals = [
              'pako/lib/zlib/inflate',
              'pako/lib/utils/common',
              'pako/lib/utils/strings',
              'pako/lib/zlib/constants',
              'pako/lib/zlib/messages',
              'pako/lib/zlib/zstream',
              'pako/lib/zlib/gzheader'
            ];
            console.log("Using builtins", entryPath);
            console.log("ignoring", resolved);
            const bundler = browserify(entryPath, {
              // this will also force
              // browserify to use native modules straight from
              // node and not provide any builtins this significantly
              // reduces the package size.
              detectGlobals: false,
              ignoreMissing: true,
              browserField: false,
              standalone: 'your-cool-lib',
              builtins: {},
            });

            for( var i = 0; i < resolved.length; i++ ){
              bundler.external(resolved[i]);
            }
            bundler.transform(
              customBabelify.configure({
                experimental: true,
                stage: 0
              })
            ).bundle(async function(err, buff){

              if( err ){
                console.error(err);
                return res.json({ status : 500 });
              }

              console.log("Attempting compression");
              const buffStr = buff.toString('utf8'); // + '\nmodule.exports = require("runner")';
              const compressedBuff = compressCode(buffStr);
              console.log("compressed now write");
              await fs.mkdirAsync(bundleDir);
              await fs.writeFileAsync(bundlePath + '-debug.js', buffStr);
              await fs.writeFileAsync(bundlePath, compressedBuff);
              return res.json({ status: 200, message: 'Ok' });
            });

      });


    }catch(e){
      console.error(e);
      res.json({ status: 500, details: e});
    }
  }
});

app.post('/compile/',async function (req, res){
  const {src, type, transpile} = req.body;

  // Is this a github repo ?
  // we cannot clone yet so we have to rely on gh or gitlab
  // for remote zipped package.
  if( type === 'repo' ){
    try{
      const dirPath = await temp.makeDirAsync('repoName');
      // once this is don lets roll!
      await getGitRepository(src);
    }catch(e){
      res.error(e);
    }
  }
});

export default app;
