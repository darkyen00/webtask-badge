import fs from 'fs';
import path from 'path';
import LRU from 'lru-cache';
import geit from './gitClient';
import Promise from 'bluebird';

Promise.promisifyAll(fs);

async function makeCache(dirPath){
  const cp = path.join(dirPath, 'cache');
  const lru = LRU({
    length: (n) => n.length,
    max: 1024 * 1024 * 10
  });

  await fs.mkdirAsync(path.join(dirPath, 'cache'));
  return {
    get: async function(key, encoding='utf8'){
      const kp = path.join(cp, key);
      const cached = lru.get(kp);
      if( cached ) return cached;
      const d = await fs.readFileAsync(kp, encoding);
      return d;
    },
    put: async function(key, buffer){
      const kp = path.join(cp, key);
      lru.set(kp, buffer);
      const f = fs.writeFileAsync(kp);
      return f;
    }
  }
}

async function extractTree(repo, tree, dir){
  for( let name in tree ){
    const item = tree[name];
    const pathname = path.join(dir, name);
    let blob = {};
    switch (item.mode) {
      case '040000':  // directory
        await fs.mkdirAsync(pathname);
        await extractTree(repo, item.children, pathname);
        break;
      case '120000':  // symbolic link
        blob = await repo.blob(item.object);
        fs.linkSync(pathname, blob.toString());
        break;

      case '160000':  // submodule
        await fs.mkdirAsync(pathname);
        // we do not support submodule yet, do we ?
        break;
      default:
        let mode = parseInt(item.mode.slice(-4), 8); // permissions
        blob = await repo.blob(item.object);
        await fs.writeFileAsync(pathname, blob, { mode: mode });
        break;
    }
  }
}

export default async function getGitRepository(repoSrc, localPath, treeName='HEAD'){
  const repo = geit(repoSrc, {
    db: await makeCache(localPath)
  });

  const tree = await repo.tree(treeName);
  await extractTree(repo, tree, localPath);

  return true;
}
